import random
import datetime

from .receiver import Receiver


class RX_Random(Receiver):
    prefix = "random"

    def __init__(self):
        super().__init__()

    def update(self):
        self.data = []
        for i in range(3):
            self.data.append({"event": random.random()})

        self.update_last = datetime.datetime.now()

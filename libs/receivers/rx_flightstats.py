import sys
import datetime
import time
import json
import shelve
import requests

# from flightaware.client import Client as fa_client
from configparser import RawConfigParser
# from miniupnpc import UPnP

from .receiver import Receiver
# from .server import JSONServer

"""
https://developer.flightstats.com/api-docs/flightstatus/v2/airport

https://developer.flightstats.com/api-docs/flightstatus/v2/flightsNear
    /v2/json/flightsNear/{lat}/{lon}/{miles}
/v2/json/flightsNear/25.254997/55.364278/50

curl -v  -X GET "https://api.flightstats.com/flex/flightstatus/rest/v2/json/\
flightsNear/25.254997/55.364278/50?appId=app_id&appKey=api_key&maxFlights=5"

"""


class RX_FlightStats(Receiver):
    port = 13372
    update_limit = 15
    data = None
    track = None  # should we look up flight tracks for flights?

    costs = [
        0.,
        0.0010,   # flightsnear, track by flight, track by airport
        0.0030,   # airlines, airports, weather
        0.0040,   # delay index, status by flight, status by route, scheudles
        # by route or airport, FIDs
        0.0008
    ]
    cost_running = 0.
    queries_running = 0

    def __init__(self, iatacode="DXB", ident="UAE", radius=0.1,
                 update_type="arrivals",
                 update_limit=update_limit,
                 track=False):
        self.update_throttle = 30  # Once a minute
        self.update_last = datetime.datetime.now() -\
            datetime.timedelta(seconds=self.update_throttle)
        self.start_time = datetime.datetime.now()

        self.iatacode = iatacode
        self.ident = ident
        self.radius = radius
        self.update_type = update_type

        # These things control lookups, and therefore, costs.
        self.update_limit = update_limit
        self.track = track

        creds = RawConfigParser()
        creds.read("credentials.cfg")

        username = creds.get("flightaware settings", "username")
        api_key = creds.get("flightaware settings", "api_key")

        self.config = shelve.open("data/fa-storage")
        if "cost_total" in self.config:
            cost_total = self.config["cost_total"]
        else:
            self.config["cost_total"] = 0.
            cost_total = 0.
        if not "queries_total" in self.config:
            self.config["queries_total"] = 0

        self.config.close()
        sys.stdout.write("FA: Current total cost: $USD%f.\n" % cost_total)

        self.source = fa_client(username, api_key)

        # Generate some variables for some types of updates
        if self.update_type == "region":
            airport = self._get_airport(self.iatacode)
            self.region = "%f %f %f %f" % (
                airport["latitude"] - radius,
                airport["latitude"] + radius,
                airport["longitude"] - radius,
                airport["longitude"] + radius
            )
            self.prefix = ["flight", "ident"]
        elif self.update_type == "airline":
            self.prefix = ["flight", "ident"]
        elif self.update_type == "arrivals":
            self.prefix = ["flight", "arrival", "ident"]
        elif self.update_type == "push":
            self.prefix = ["flight", "eventcode", "ident"]
            # Clear all old alerts (in case that hasn't been done)
            self._clear_pushes()

            self._run_server()

            # Set the push registration end point
            success = self.source.register_alert_endpoint(
                "http://%s:%s/" % (self.public_addr, self.port))
            self._update_cost(0)
            if success != 1:
                sys.stderr.write("FA: Could not register alert endpoint.\n")
                raise Exception('FA', 'Could not register an Alert endpoint.')
            else:
                # Set the alert
                t1 = datetime.datetime.utcnow()
                t2 = t1 + datetime.timedelta(days=1)
                # ts = int(time.mktime(t.timetuple()))
                alert = self.source.set_alert(
                    date_start=int(time.mktime(t1.timetuple())),
                    date_end=int(time.mktime(t2.timetuple())),
                    destination=iatacode,
                    channels="{16 e_arrival e_departure}",
                    max_weekly=10000
                )
                self._update_cost(3)
                sys.stderr.write("FA: Alert %s to %s:%s.\n" % (
                                 alert, self.public_addr, self.port))

                # Start the push server
                # Currently manually done
                # Listen for push notifications
        sys.stdout.write("FA: Listening for %s from %s.\n" %
                        (self.update_type, self.iatacode))

    def update(self):
        # Only do this every throttle seconds
        if datetime.datetime.now() < self.update_last \
                + datetime.timedelta(seconds=self.update_throttle):
            return
        sys.stdout.write("FA: Updating...\n")

        if self.update_type == "arrivals":
            self._update_arrivals()
        elif self.update_type == "arrivalsanddepartures":
            pass
        elif self.update_type == "region":
            self._update_region()
        elif self.update_type == "airline":
            self._update_airline()
        elif self.update_type == "push":
            self._update_push()
        sys.stdout.write("FA: Update done.\n")
        self.update_last = datetime.datetime.now()
        sys.stdout.write("FA: Running total: $%f.\n" % self.cost_running)

        seconds = (self.update_last - self.start_time).total_seconds()
        cost_per_day = self.cost_running / seconds * 60*60*24
        queries_per_day = self.queries_running / seconds * 60*60*24
        sys.stdout.write("FA: Daily estimate (%d queries): $%f.\n" %
                        (queries_per_day, cost_per_day))

    def _update_cost(self, c):
        self.cost_running += self.costs[c]
        self.queries_running += 1

    def _update_arrivals(self):
        sys.stderr.write("FA: Getting arrivals into %s.\n" % self.iatacode)
        # this costs about USD0.134 every time it runs (DXB)
        # ~240 records, / 15 (each query), * Class 2 (0.0078USD)
        # http://flightaware.com/commercial/flightxml/pricing_class.rvt
        # DXB has about 10 arrivals an hour
        next_offset = 0
        flights = []

        while next_offset > -1:
            sys.stderr.write("FA: Getting %d to %d arrived into %s.\n" %
                            (next_offset, next_offset + 15, self.iatacode))
            data = self.source.arrived(self.iatacode, offset=next_offset)
            self._update_cost(2)
            flights = flights + data.get("arrivals")

            if self.update_limit and len(flights) >= self.update_limit:
                break
            next_offset = data.get("next_offset")

        self.data = flights
        # sys.stdout.write(json.dumps(self.data, indent=4, sort_keys=True))
        # sys.stdout.write(type(self.data) is list)

    # We can upgrade to SearchBirdseyeInFlight to get extra functions
    def _update_region(self):
        sys.stdout.write("FA: Getting flights within %s\n" % self.region)

        arguments = {
            "latlong": "\"%s\"" % self.region,
            "inAir": 1
        }
        # Search might only search flights occuring now (rather than 24hrs)
        # SearchBirdseyeInFlight might be useful using lastPositionTime
        next_offset = 0
        flights = []

        while next_offset > -1:
            result = self.source.search(arguments)
            self._update_cost(2)
            flights = flights + result.get("aircraft")

            if self.update_limit and len(flights) >= self.update_limit:
                break
            next_offset = result.get("next_offset")

        flights = self._after_data(flights)
        self.data = flights
        # sys.stdout.write(json.dumps(self.data, indent=4, sort_keys=True))
        # sys.stdout.write(type(self.data) is list)

    # We can upgrade to SearchBirdseyeInFlight to get extra functions
    def _update_airline(self):
        arguments = {
            "inAir": 1,
            "idents": "%s*" % self.ident
        }
        next_offset = 0
        flights = []
        while next_offset > -1:
            result = self.source.search(arguments)
            self._update_cost(2)
            flights = flights + result.get("aircraft")

            if self.update_limit and len(flights) >= self.update_limit:
                break
            next_offset = result.get("next_offset")

        self.data = flights

    """
    Nothing to do here. Push notifications will come in the other way.
    """
    def _update_push(self):
        if self.server.isRunning():
            print("FA: Server still running.")
        else:
            print("FA: ****Server stopped****")
            self.close()

    """
    Clear all existing push alerts.
    """
    def _clear_pushes(self):
        res = self.source.get_alerts()
        self._update_cost(3)

        for alert in res["alerts"]:
            if self.source.delete_alert(alert["alert_id"]) > 0:
                self._update_cost(3)
                sys.stdout.write("FA: Cancelled alert %d.\n" %
                                 alert["alert_id"])

    def _push_callback(self, data):
        print("FA: Callback called.")
        print(json.dumps(data, indent=2))
        print('\a')
        # Each callback is a class 2 query.
        self._update_cost(2)

        # Set the data
        if not self.data:
            self.data = []
        data["flight"]["eventcode"] = data["eventcode"]
        self.data.append(data["flight"])

    """
    Run a listening server on the port.
    """
    def _run_server(self):
        try:
            self._open_port()
        except:
            sys.stdout.write("Couldn't open port %d. Please open the port\
                manually.\n" % self.port)
            self._get_public_addr()

        self.server = JSONServer(self.port, self._push_callback)
        self.server.run()

    def _stop_server(self):
        self.server.stop()

    """
    Open a UPnP port for the server.
    """
    def _open_port(self):
        self.proto = "TCP"

        # Open a port
        u = UPnP()
        ndevices = u.discover()
        try:
            u.selectigd()
        except:
            raise Exception('FA', 'Could not open port on %s. No UPnP device \
                found.'
                            % self.port)

        # Get our IP address
        self.public_addr = u.externalipaddress()
        self.private_addr = u.lanaddr

        success = u.addportmapping(self.port, self.proto,
                                   self.private_addr, self.port,
                                   'FlightAware %s at %s:%s' %
                                   (self.proto, self.private_addr,
                                    self.port), '')

        if success:
            sys.stderr.write("FA: Opened port %s on %s to %s.\n" % (
                             self.port, self.public_addr,
                             self.private_addr))
        else:
            raise Exception('FA', 'Could not open port on %s.' % self.port)

    def _close_port(self):
        u = UPnP()
        ndevices = u.discover()
        try:
            u.selectigd()
            u.deleteportmapping(self.port, self.proto)
        except:
            sys.stdout.write("You can now close port %d.\n" % self.port)

    def _get_public_addr(self):
        r = requests.get(r'http://jsonip.com')
        self.public_addr = r.json()['ip']

    """
    Get airport information by IATA iatacode.
    """
    def _get_airport(self, iatacode):
        # This data structure could be shelved along with the other data
        try:
            airports = json.load(open("data/fa-airports.json", "rb"))
            json.dumps(airports, indent=2)
            sys.stderr.write("%d airports loaded.\n" % len(airports))
        except:
            sys.stderr.write("Airports file will be created.\n")
            airports = {}

        if iatacode not in airports:
            sys.stderr.write("Looking up %s.\n" % iatacode)
            info = self.source.airport_info(iatacode)
            self._update_cost(3)

            # save it to our local database
            airports[iatacode] = info

            with open("data/fa-airports.json", 'w') as outfile:
                json.dump(airports, outfile, indent=2)

        return airports[iatacode]

    """
    Transformations to perform on the data before allowing it out.
    """
    def _after_data(self, data):
        if self.track:
            for d in data:
                if not d["waypoints"] and d["faFlightID"]:
                    # fid = d["faFlightID"]
                    # or ident@departureTime
                    d["waypoints"] = self.get_waypoints(d)
        return data

    """
    Utilities

    * DecodeFlightRoute: From an FID get the proposed route (not actual).
    """

    def get_waypoints(self, flight):
        """
        @TODO: What we could do is store our own waypoints over time rather
        than loading expensive points here.
        """
        # fid = flight["faFlightID"]
        # for decode_flight_route
        # fid = "%s@%s" % (flight["ident"], flight["departureTime"])
        # for in_flight_info and get_last_track
        fid = flight["ident"]
        # returns "no data found"
        # data = self.source.decode_flight_route(fid)
        # returns "" for waypoints outside the US
        # data = self.source.in_flight_info(fid)
        # for in air returns the track so far
        data = self.source.get_last_track(fid)
        self._update_cost(2)
        return data

    def stop(self):
        if self.update_type == "push":
            self._clear_pushes()
            self._stop_server()
            self._close_port()

        sys.stdout.write("FA: Running cost on %d queries: $%f.\n" %
                        (self.queries_running, self.cost_running))
        self.config = shelve.open("data/fa-storage")
        self.config["cost_total"] += self.cost_running
        self.config["queries_total"] += self.queries_running
        sys.stdout.write("FA: Total cost on %d queries: $USD%f.\n" %
                        (self.config["queries_total"],
                         self.config["cost_total"]))

        self.config.close()


def main():
    fa = RX_FlightAware()

    while True:
        fa.update()
        sys.stdout.write("%s\n" % fa.update_last)
        time.sleep(fa.update_throttle)


if __name__ == "__main__":
    main()

#!/bin/bash

PROJECT=spacefolding

source $WORKON_HOME/$PROJECT/bin/activate

cd `cat $WORKON_HOME/$PROJECT/.project`
python3 spacefolding.py

import os
import json
import xml.dom.minidom

# flightaware json to kml
# http://discussions.flightaware.com/post132997.html
# PHP code sample here
# http://discussions.flightaware.com/post140931.html

directory = "data/tracks"
altitude_factor = 250.0
linecolour = "99ffffff"
fillcolour = "66669966"


def get_coords(data):
    # print(json)
    track = json.loads(data)
    coords = []
    for point in track:
        try:
            coords.append("%f,%f,%f" % (point['longitude'], point['latitude'], point['altitude'] * altitude_factor))
        except:
            pass
    return " ".join(coords)


def main():
    xmldoc = xml.dom.minidom.Document()

    kml = xmldoc.createElementNS("http://www.opengis.net/kml/2.2", "kml")
    kml.setAttribute("xmlns", "http://www.opengis.net/kml/2.2")
    xmldoc.appendChild(kml)

    doc = xmldoc.createElement("Document")
    kml.appendChild(doc)

    # Add style to do
    """
        <Style id="yellowLineGreenPoly">
            <LineStyle>
                <!-- ABGR -->
                <color>99ffffff</color>
            </LineStyle>
            <PolyStyle>
                <color>66669966</color>

          <poly_fill>
            1
          </poly_fill>
            </PolyStyle>
        </Style>
    """
    style = xmldoc.createElement("Style")
    style.setAttribute("id", "default")
    linestyle = xmldoc.createElement("LineStyle")
    polystyle = xmldoc.createElement("PolyStyle")
    l_colour = xmldoc.createElement("color")
    p_colour = xmldoc.createElement("color")
    l_colour.appendChild(xmldoc.createTextNode(linecolour))
    p_colour.appendChild(xmldoc.createTextNode(fillcolour))

    linestyle.appendChild(l_colour)
    polystyle.appendChild(p_colour)
    style.appendChild(linestyle)
    style.appendChild(polystyle)

    doc.appendChild(style)

    # iterate over directory for json files
    for filename in os.listdir(directory):
        if filename.endswith(".json"):

            path = "%s/%s" % (directory, filename)
            f = open(path, "r")
            # print(f)
            data = f.read()
            # airports = json.load(open("data/fa-airports.json", "rb"))

            placemark = xmldoc.createElement("Placemark")

            name = xmldoc.createElement("name")
            name.appendChild(xmldoc.createTextNode(filename))
            placemark.appendChild(name)

            style = xmldoc.createElement("styleUrl")
            style.appendChild(xmldoc.createTextNode("#default"))
            placemark.appendChild(style)

            linestring = xmldoc.createElement("LineString")

            extrude = xmldoc.createElement("extrude")
            extrude.appendChild(xmldoc.createTextNode("1"))
            linestring.appendChild(extrude)

            altmode = xmldoc.createElement("altitudeMode")
            altmode.appendChild(xmldoc.createTextNode("relativeToGround"))
            linestring.appendChild(altmode)

            coords = xmldoc.createTextNode(get_coords(data))
            coordinates = xmldoc.createElement("coordinates")
            coordinates.appendChild(coords)
            linestring.appendChild(coordinates)

            placemark.appendChild(linestring)

            doc.appendChild(placemark)

    # xml = xml.dom.minidom.parse(xml_fname) # or xml.dom.minidom.parseString(xml_string)
    pretty_xml_as_string = xmldoc.toprettyxml()
    print(pretty_xml_as_string)


main()

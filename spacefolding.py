"""
Options for eventing

* Multiprocessing and Queue
* threading and Event
* pysubpub
* Greenlets
* Twisted
"""
# from time import sleep
# import thread
import logging
import time
from multiprocessing import Process, Event
import threading
import sys
# import json
# import datetime
# import asyncio

from configparser import RawConfigParser

from libs.receivers.rx_random import RX_Random
from libs.receivers.rx_logger import RX_Logger
from libs.receivers.rx_flightaware import RX_FlightAware
from libs.transmitters.tx_osc import TX_OSC
from libs.transmitters.tx_logger import TX_Logger
# from libs.transmitters.osc import OSC

logging.basicConfig(
    filename='logs/spacefolding-data.log',
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s'
)


class SpaceFolding():
    running = False
    events = {}
    rx = None
    tx = None

    def __init__(self):
        self.events["target_update"] = Event()

#        try:
#            arduino = Arduino()
#            arduino.run()
#        except:
#            logging.error("Can't connect to Arduino.")

        self.running = True
        self.begin()

    def begin(self):
        self.rx = MetaReceiver(events=self.events)
        self.rx.start()

        # self.tx = None
        self.tx = MetaTransmitter()
        self.tx.start()

        while self.running:
            try:
                #   sleep a little so other processes can have a turn
                if self.rx and self.rx.update.isSet():
                    data, prefix = self.rx.getUpdate()
                    self.rx.update.clear()
                    logging.debug(data)
                    if type(data) is list and len(data) > 0:
                        if self.tx is not None:
                            self.tx.setData(data, prefix)

                time.sleep(0.01)
            except KeyboardInterrupt:
                logging.debug("Cleaning up spacefolding.")
                if self.rx:
                    self.rx.stop()
                    # self.rx.join()
                if self.tx:
                    self.tx.stop()
                    # self.tx.join()
                self.destroy()
                self.running = False

    def destroy(self):
        self.running = False
        logging.debug("Exit on next line")
        sys.exit(0)


class MetaTransmitter(threading.Thread):
    events = None
    interval = 0.1
    transmitters = {}
    data = None
    prefix = None

    def __init__(self, thread_name=None, events={}):
        threading.Thread.__init__(self)
        self.stopping = threading.Event()
        self.update = threading.Event()

        self.transmitters['osc'] = TX_OSC()
        # self.transmitters['logger'] = TX_Logger(
        #     filename="data/fa-region-waypoints-dxb.log")

    def setData(self, data, prefix=None):
        self.data = data
        self.prefix = prefix

    def sendUpdates(self):
        if not self.data:
            return
        for name in self.transmitters:
            transmitter = self.transmitters[name]
            transmitter.setData(self.data, self.prefix)
        self.data = None

    def run(self):
        while True:
            if self.stopped():
                self.cleanup()
                return

            self.sendUpdates()

            try:
                # @TODO Too long so won't interrupt nicely
                time.sleep(self.interval)
            except KeyboardInterrupt:
                self.stop()

    def cleanup(self):
        logging.debug("Transmitters cleaning up.")
        for l in self.transmitters:
            self.transmitters[l].stop()

    def stop(self):
        self.stopping.set()

    def stopped(self):
        try:
            return self.stopping.is_set()
        except:
            return True


class MetaReceiver(threading.Thread):
    """
        A handler for all the kinds of listeners that we want to hear from.
        Look at the way bloodbath did eventing.
    """
    events = None
    interval = 0.1
    receivers = {}

    def __init__(self, thread_name=None, events={}):
        config = RawConfigParser({})
        config.readfp(open('defaults.cfg'))
        config.read("credentials.cfg")

        iatacode = config.get("spacefolding settings", "iatacode")
        ident = config.get("spacefolding settings", "ident")
        radius = float(config.get("spacefolding settings", "radius"))
        track = int(config.get("spacefolding settings", "track"))
        update_limit = int(config.get("spacefolding settings", "update_limit"))
        update_type = [x.strip() for x in config.get("spacefolding settings", "update_type").split(',')]
        update_throttle = int(config.get("spacefolding settings", "update_throttle"))

        print(track)
        if track:
            print("Tracking enabled. This might be costly if you've removed your cache.")

        threading.Thread.__init__(self)

        self.stopping = threading.Event()
        self.update = threading.Event()

        # self.receivers['rnd'] = RX_Random()
        # this is currently not working
        # self.receivers['logger'] = RX_Logger(
        #     "data/fa-region-waypoints-dxb.log",
        #     prefix=["flight", "ident"]
        # )
        for u in update_type:
            # override defaults with type specific settings
            # @TODO fix this so its not fugly
            try:
                iatacode = config.get("%s settings" % u, "iatacode")
                print("FA: Overrode iatacode for '%s' with '%s'." % (u, iatacode))
            except:
                pass
            try:
                ident = config.get("%s settings" % u, "ident")
                print("FA: Overrode ident for '%s' with '%s'." % (u, ident))
            except:
                pass
            try:
                radius = float(config.get("%s settings" % u, "radius"))
                print("FA: Overrode radius for '%s' with '%s'." % (u, radius))
            except:
                pass
            try:
                track = int(config.get("%s settings" % u, "track"))
                print("FA: Overrode track for '%s' with '%s'." % (u, track))
            except:
                pass
            try:
                update_limit = int(config.get("%s settings" % u, "update_limit"))
                print("FA: Overrode update_limit for '%s' with '%s'." % (u, update_limit))
            except:
                pass
            try:
                update_throttle = int(config.get("%s settings" % u, "update_throttle"))
                print("FA: Overrode update_throttle for '%s' with '%s'." % (u, update_throttle))
            except:
                pass

            self.receivers["fa-%s" % u] = RX_FlightAware(
                iatacode=iatacode, ident=ident, update_type=u, radius=radius,
                update_limit=update_limit, update_throttle=update_throttle, track=track
            )

    def eventListener(self, e, event_name):
        pass

    def getUpdate(self):
        # The main program is asking for updates from the receiver
        return [self.data, self.prefix]

    def cleanup(self):
        logging.debug("Receivers cleaning up.")
        for l in self.receivers:
            self.receivers[l].stop()

    def run(self):
        while True:
            if self.stopped():
                self.cleanup()
                return
            for name in self.receivers:
                receiver = self.receivers[name]
                # calls the update hook
                receiver.update()
                if receiver.data:
                    # if we get something back then set the update event handle
                    self.name = name
                    self.data = receiver.data
                    if receiver.prefix:
                        self.prefix = receiver.prefix
                    receiver.data = None

                    self.update.set()
            try:
                # @TODO Too long so won't interrupt nicely
                time.sleep(self.interval)
            except KeyboardInterrupt:
                self.stop()

    # http://stackoverflow.com/questions/323972/
    # is-there-any-way-to-kill-a-thread-in-python
    def stop(self):
        self.stopping.set()

    def stopped(self):
        return self.stopping.is_set()


def main():
    spacefolding = SpaceFolding()


if __name__ == "__main__":
    main()

import sys
import time
import json

import logging

from pythonosc import osc_message_builder
from pythonosc import udp_client

# Testing
import random

from .transmitter import Transmitter


class TX_OSC(Transmitter):
    ip = "127.0.0.1"
    port = 8000

    # r = 0.5

    def __init__(self, ip=ip, port=port):
        super().__init__()

        self.update_throttle = 1.01
        self.ip = ip
        self.port = port
        self.sender = udp_client.UDPClient(self.ip, self.port)

    def send(self):
        # is there any data?
        # if not len(self.data):
        #     super().send()
        #     return
        if not self.data:
            return

        # print(self.data)
        # an array of objects or an object
        try:
            for d in self.data:
                # the object
                # is there an address prefix?
                prefix = []
                if self.prefix:
                    for p in self.prefix:
                        # is the prefix a key in the data structure?
                        if p in d:
                            prefix.append(d[p])
                        else:
                            prefix.append(p)
                prefix = "/".join(prefix)

                for key, value in d.items():
                    if not value:
                        continue
                    elif type(value) is list:
                        # value = " ".join(value)
                        continue
                    elif type(value) is dict:
                        continue
                    elif type(value) is None:
                        continue

                    if prefix:
                        addr = "/".join([prefix, key])
                        if "latitude" in addr and type(value) != float:
                            print(addr)
                            print(type(value))
                            print(value)
                            continue
                        elif "longitude" in addr and type(value) != float:
                            print(addr)
                            print(type(value))
                            print(value)
                            continue
                    else:
                        addr = key

                    msg = osc_message_builder.OscMessageBuilder(
                        address="/%s" % addr)
                    # @TODO value might be an array or a string
                    msg.add_arg(value)
                    msg = msg.build()
                    try:
                        self.sender.send(msg)
                    except AttributeError:
                        sys.stderr.write("AttributeError: %s.\n"
                                         % sys.exc_info()[1])
                        raise
                    except:
                        sys.stderr.write("Failed to send OSC: %s.\n"
                                         % sys.exc_info()[0])
                        raise

        except:
            sys.stderr.write("Failed to send any data.\n%s\n"
                             % sys.exc_info()[1])
            logging.debug(self.data)

        # Clear the data
        super().send()

    def stop(self):
        super().stop()


def main():
    tx = TX_OSC(port=8000)

    while tx.running:
        try:
            tx.setData([{"event": random.random()}])
            tx.send()
            time.sleep(tx.update_throttle)
        except KeyboardInterrupt:
            tx.stop()
            sys.exit(0)


if __name__ == "__main__":
    main()

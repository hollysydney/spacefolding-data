import sys
import time
import datetime


class Transmitter():
    update_throttle = 3
    update_last = datetime.datetime.now() -\
        datetime.timedelta(seconds=update_throttle)
    running = False
    data = None

    def __init__(self):
        self.running = True

    def update(self):
        self.send()
        self.update_last = datetime.datetime.now()

    def setData(self, data, prefix=None):
        self.data = data
        self.prefix = prefix
        self.send()

    def send(self):
        self.data = None

    def stop(self):
        self.running = False

    def set_update_throttle(self, update_throttle):
        self.update_throttle = update_throttle


def main():
    tx = Transmitter()

    while tx.running:
        tx.update()
        sys.stdout.write("%s\n" % tx.update_last)
        time.sleep(tx.update_throttle)


if __name__ == "__main__":
    main()

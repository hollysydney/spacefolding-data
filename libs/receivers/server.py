import socketserver
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
# import cgi
import time
# from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
# import socketserver
import json
import sys

# https://gist.github.com/bradmontgomery/2219997
# http://stackoverflow.com/questions/17888504/\
# python-basehttprequesthandler-read-raw-post


class JSONRequestHandler(BaseHTTPRequestHandler):
    data = {}

    def __init__(self, callback, *args, **keys):
        self.callback = callback
        socketserver.BaseRequestHandler.__init__(self, *args, **keys)

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()

    def do_GET(self):
        self._set_headers()

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # application/json; charset=utf-8
        # contenttype = self.headers.getheader('content-type')
        contenttype = self.headers.get("Content-Type")
        length = self.headers.get('Content-Length')
        sys.stderr.write("Received %s bytes of %s.\n" % (length, contenttype))

        post_data = self.rfile.read(int(length))
        post_data = post_data.decode("utf-8")

        self.data = json.loads(post_data)
        self.callback(self.data)

    def getUpdate(self):
        return self.data


# http://stackoverflow.com/questions/5480456/proper-way-to-add-functionality-to
# -handle-in-a-python-tcp-server
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


class JSONServer():
    running = False

    def __init__(self, port=13372, callback=None):
        self.port = port
        self.callback = callback

    def run(self):
        server_address = ('', self.port)

        server = ThreadedTCPServer(
            server_address, lambda *args, **keys:
            JSONRequestHandler(
                self.callback, *args, **keys)
        )

        self.server_thread = threading.Thread(target=server.serve_forever)
        self.server_thread.daemon = True
        self.server_thread.start()
        sys.stdout.write("Server running on %d.\n" % self.port)

        self.running = True

    def isRunning(self):
        return self.server_thread.isAlive()

    def stop(self):
        self.running = False


def my_callback(data):
    print("Callback called.")
    print(json.dumps(data, indent=2))


if __name__ == "__main__":
    from sys import argv

    server = JSONServer(callback=my_callback)

    if len(argv) == 2:
        server.port = int(argv[1])

    server.run()

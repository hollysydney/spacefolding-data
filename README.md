#Space Folding#

##Install

```
$ brew install python3
$ pip3 install virtualenvwrapper
```

Configure virtualenv
```
$ cat ~/.profile

VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh

$ mkvirtualenv spacefolding
```

###
###Install dependencies

`(spacefolding) $ pip3 install -r requirements.txt`

###Create data dir

`$ mkdir data`

###Create config file

`cat credentials.cfg`

```
[spacefolding settings]
; which airport are we interested in?
iatacode = DXB
; which airline are we interested in?
ident = UAE
; some number of arrivals, region, push, airline
update_type = arrivals,region
; for regions what is the radius of the box in degrees?
radius = 0.1

[flightaware settings]
; http://flightaware.com/commercial/flightxml/key
username = xxxxxxxx
api_key = xxxxxxxx
```

##Upgrading python
`$ brew update && brew upgrade python3`

Data sources

* Flightaware API wrapper https://github.com/fredpalmer/flightaware (upgrade to v2?)

Visualisation

* Kivy app development

Lighting control

* Abstract lighting control using OpenLightingProject https://github.com/OpenLightingProject/ola
* ARTNET DMX over ethernet
* NDB DDP controller http://www.3waylabs.com/ddp/

uPnP

Can use pip
pip3 miniupnpc fails (try to rebuild)
https://github.com/chenhouwu/miniupnpc

* Clone miniupnp
* Build
* `make && sudo make install`
* Build python
* `make pythonmodule3`
* `source ~/.profile`
* `workon spacefolding`
* `make installpythonmodule3`
`>>> import miniupnpc`


# @TODO

* OSC should have better paths.
* Figure out which is cheaper in FA; queries or callbacks.

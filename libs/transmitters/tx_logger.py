import json
import logging

from .transmitter import Transmitter


class TX_Logger(Transmitter):

    def __init__(self, filename="data/spacefolding.log"):
        super().__init__()
        logging.basicConfig(
            filename=filename,
            level=logging.INFO,
            format='%(asctime)s %(message)s',
            datefmt='%Y-%m-%d-%H:%M:%S'
        )

    def send(self):
        if not self.data:
            return

        logging.info(json.dumps(self.data))
        super().send()

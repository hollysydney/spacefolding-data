import sys
import datetime
import time


class Receiver():
    running = False
    update_throttle = 1.0
    update_last = datetime.datetime.now() -\
        datetime.timedelta(seconds=update_throttle)
    data = {}

    def __init__(self):
        self.running = True

    def getUpdate(self):
        if type(self.data is list) and len(self.data) > 0:
            d = self.data
            self.data = []
            return d
        else:
            return None

    def update(self):
        # Only do this every throttle seconds
        if datetime.datetime.now() < self.update_last \
                + datetime.timedelta(seconds=self.update_throttle):
            return
        sys.stdout.write("Receiver: updating...")

        self.update_last = datetime.datetime.now()

    def stop(self):
        pass


def main():
    rx = Receiver()

    while True:
        rx.update()
        sys.stdout.write("%s\n" % rx.update_last)
        time.sleep(rx.update_throttle)


if __name__ == "__main__":
    main()

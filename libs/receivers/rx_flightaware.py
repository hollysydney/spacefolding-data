import sys
import os
import datetime
import time
import json
import shelve
import requests

import logging

from flightaware.client import Client as fa_client
from configparser import RawConfigParser
from miniupnpc import UPnP

from .receiver import Receiver
from .server import JSONServer

"""
TODO
To get a path (of navaids / waypoints) call DecodeFlightRoute with an FID.
Note this is planned not actual route


* Region: Returns all in air aircraft near a given airport.
* Push: Returns (via push notification) arrivals and departures from a given
airport.
"""

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class RX_FlightAware(Receiver):
    port = 13372
    update_limit = 15
    update_throttle = 30
    data = None
    track = None  # should we look up flight tracks for flights?

    costs = [
        0.,
        0.0120,
        0.0079,
        0.0020,
        0.0008
    ]
    cost_running = 0.
    queries_running = 0

    def __init__(self, iatacode="DXB", ident="UAE", radius=0.1,
                 update_type="arrivals",
                 update_limit=update_limit,
                 update_throttle=update_throttle,
                 track=False):
        self.update_throttle = update_throttle
        self.update_last = datetime.datetime.now() -\
            datetime.timedelta(seconds=self.update_throttle)
        self.start_time = datetime.datetime.now()

        self.iatacode = iatacode
        self.ident = ident
        self.radius = radius
        self.update_type = update_type

        # These things control lookups, and therefore, costs.
        self.update_limit = update_limit
        self.track = track

        creds = RawConfigParser()
        creds.read("credentials.cfg")

        username = creds.get("flightaware settings", "username")
        api_key = creds.get("flightaware settings", "api_key")

        self.config = shelve.open("data/fa-storage")
        if "cost_total" in self.config:
            cost_total = self.config["cost_total"]
        else:
            self.config["cost_total"] = 0.
            cost_total = 0.
        if not "queries_total" in self.config:
            self.config["queries_total"] = 0

        self.config.close()
        logging.info("FA: Current total cost: $USD%f." % cost_total)

        self.source = fa_client(username, api_key)

        # Generate some variables for some types of updates
        if self.update_type == "region":
            airport = self._get_airport(self.iatacode)
            try:
                self.region = "%f %f %f %f" % (
                    airport["latitude"] - radius,
                    airport["latitude"] + radius,
                    airport["longitude"] - radius,
                    airport["longitude"] + radius
                )
                self.prefix = ["flight", "ident"]
            except:
                print(airport)
        elif self.update_type == "airline":
            self.prefix = ["flight", "ident"]
        elif self.update_type == "arrivals":
            self.prefix = ["flight", "arrival", "ident"]
        elif self.update_type == "departures":
            self.prefix = ["flight", "departures", "ident"]
        elif self.update_type == "push":
            self.prefix = ["flight", "eventcode", "ident"]
            # Clear all old alerts (in case that hasn't been done)
            self._clear_pushes()

            self._run_server()

            # Set the push registration end point
            success = self.source.register_alert_endpoint(
                "http://%s:%s/" % (self.public_addr, self.port))
            self._update_cost(0)
            if success != 1:
                sys.stderr.write("FA: Could not register alert endpoint.\n")
                raise Exception('FA', 'Could not register an Alert endpoint.')
            else:
                # Set the alert
                t1 = datetime.datetime.utcnow()
                t2 = t1 + datetime.timedelta(days=1)
                # ts = int(time.mktime(t.timetuple()))
                alert = self.source.set_alert(
                    date_start=int(time.mktime(t1.timetuple())),
                    date_end=int(time.mktime(t2.timetuple())),
                    destination=iatacode,
                    channels="{16 e_arrival e_departure}",
                    max_weekly=10000
                )
                self._update_cost(3)
                sys.stderr.write("FA: Alert %s to %s:%s.\n" % (
                                 alert, self.public_addr, self.port))

                # Start the push server
                # Currently manually done
                # Listen for push notifications
        logging.info("FA: Listening for %s from %s." %
                        (self.update_type, self.iatacode))

    def update(self):
        # Only do this every throttle seconds
        if datetime.datetime.now() < self.update_last \
                + datetime.timedelta(seconds=self.update_throttle):
            return
        logging.info("FA: Updating %s..." % self.update_type)

        if self.update_type == "arrivals":
            self._update_arrivals()
        elif self.update_type == "departures":
            self._update_departures()
        elif self.update_type == "arrivalsanddepartures":
            pass
        elif self.update_type == "region":
            self._update_region()
        elif self.update_type == "airline":
            self._update_airline()
        elif self.update_type == "push":
            self._update_push()
        logging.info("FA: Update %s done." % self.update_type)
        self.update_last = datetime.datetime.now()
        nextrun = self.update_last + datetime.timedelta(seconds=self.update_throttle)
        logging.info("FA-%s: Won't run again until %s (%d s)." % (self.update_type, nextrun.strftime("%H:%M:%S"), self.update_throttle))
        logging.info("FA: Running total: $%f." % self.cost_running)

        seconds = (self.update_last - self.start_time).total_seconds()
        cost_per_day = self.cost_running / seconds * 60*60*24
        queries_per_day = self.queries_running / seconds * 60*60*24
        logging.info("FA: Daily estimate (%d queries): $%f." %
                        (queries_per_day, cost_per_day))

    def _update_cost(self, c):
        self.cost_running += self.costs[c]
        self.queries_running += 1

    def _update_arrivals(self):
        logging.info("FA-arrivals: Getting %d arrivals into %s." % (self.update_limit, self.iatacode))
        # this costs about USD0.134 every time it runs (DXB)
        # ~240 records, / 15 (each query), * Class 2 (0.0078USD)
        # http://flightaware.com/commercial/flightxml/pricing_class.rvt
        # DXB has about 10 arrivals an hour
        next_offset = 0
        flights = []

        while next_offset > -1:
            logging.debug("FA-arrivals: Getting %d to %d arrived into %s." %
                            (next_offset, next_offset + 15, self.iatacode))
            try:
                data = self.source.arrived(self.iatacode, offset=next_offset)
                self._update_cost(2)
                flights = flights + data.get("arrivals")

                if self.update_limit and len(flights) >= self.update_limit:
                    break
                next_offset = data.get("next_offset")
            except:
                sys.stderr.write("FA-arrivals: ERROR Couldn't get arrivals update.\n")
                time.sleep(1.0)

        self.message("FA-departures: Got %d flights arrived into %s." % (len(flights), self.iatacode))
        flights = self._after_data(flights)
        self.data = flights
        # logging.info(json.dumps(self.data, indent=4, sort_keys=True))
        # logging.info(type(self.data) is list)

    def _update_departures(self):
        logging.info("FA-departures: Getting %d departures from %s." % (self.update_limit, self.iatacode))
        next_offset = 0
        flights = []

        while next_offset > -1:
            try:
                logging.debug("FA-departures: Getting %d to %d departed from %s." %
                                (next_offset, next_offset + 15, self.iatacode))
                data = self.source.departed(self.iatacode, offset=next_offset)
                self._update_cost(2)
                flights = flights + data.get("departures")

                if self.update_limit and len(flights) >= self.update_limit:
                    break
                next_offset = data.get("next_offset")
            except:
                sys.stderr.write("FA-departures: ERROR Couldn't get departures update.\n")
                time.sleep(1.0)

        self.message("FA-departures: Got %d flights departed from %s." % (len(flights), self.iatacode))
        flights = self._after_data(flights)
        self.data = flights

    # We can upgrade to SearchBirdseyeInFlight to get extra functions
    def _update_region(self):
        logging.info("FA-region: Getting flights within %s" % self.region)

        arguments = {
            "latlong": "\"%s\"" % self.region,
            "inAir": 1
        }
        # Search might only search flights occuring now (rather than 24hrs)
        # SearchBirdseyeInFlight might be useful using lastPositionTime
        next_offset = 0
        flights = []

        while next_offset > -1:
            try:
                result = self.source.search(arguments)
                self._update_cost(2)
                flights = flights + result.get("aircraft")

                if self.update_limit and len(flights) >= self.update_limit:
                    break
                next_offset = result.get("next_offset")
            except:
                sys.stderr.write("FA-region: ERROR Couldn't get region update.\n")
                time.sleep(1.0)

        self.message("FA-region: Got %d flights in the %s region." % (len(flights), self.iatacode))
        flights = self._after_data(flights)
        self.data = flights
        # logging.info(json.dumps(self.data, indent=4, sort_keys=True))
        # logging.info(type(self.data) is list)

    # We can upgrade to SearchBirdseyeInFlight to get extra functions
    def _update_airline(self):
        arguments = {
            "inAir": 1,
            "idents": "{%s*}" % self.ident.replace(",", "* ")
        }
        next_offset = 0
        flights = []
        while next_offset > -1:
            try:
                result = self.source.search(arguments)
                self._update_cost(2)
                flights = flights + result.get("aircraft")
                if self.update_limit and len(flights) >= self.update_limit:
                    break
                next_offset = result.get("next_offset")
            except:
                sys.stderr.write("FA: ERROR Couldn't get airline update.\n")
                time.sleep(1.0)

        self.message("FA-airline: Got %d flights from airline %s." % (len(flights), arguments['idents']))
        flights = self._after_data(flights)
        self.data = flights

    """
    Nothing to do here. Push notifications will come in the other way.
    """
    def _update_push(self):
        if self.server.isRunning():
            print("FA: Server still running.")
        else:
            print("FA: ****Server stopped****")
            self.close()

    """
    Clear all existing push alerts.
    """
    def _clear_pushes(self):
        res = self.source.get_alerts()
        self._update_cost(3)

        for alert in res["alerts"]:
            if self.source.delete_alert(alert["alert_id"]) > 0:
                self._update_cost(3)
                logging.info("FA: Cancelled alert %d." %
                                 alert["alert_id"])

    def _push_callback(self, data):
        logging.info("FA: Callback called.")
        print(json.dumps(data, indent=2))
        print('\a')
        # Each callback is a class 2 query.
        self._update_cost(2)

        # Set the data
        if not self.data:
            self.data = []
        data["flight"]["eventcode"] = data["eventcode"]
        self._after_data([data["flight"]])
        self.data.append(data["flight"])

    """
    Run a listening server on the port.
    """
    def _run_server(self):
        try:
            self._open_port()
        except:
            logging.info("Couldn't open port %d. Please open the port\
                manually." % self.port)
            self._get_public_addr()

        self.server = JSONServer(self.port, self._push_callback)
        self.server.run()

    def _stop_server(self):
        self.server.stop()

    """
    Open a UPnP port for the server.
    """
    def _open_port(self):
        self.proto = "TCP"

        # Open a port
        u = UPnP()
        ndevices = u.discover()
        try:
            u.selectigd()
        except:
            raise Exception('FA', 'Could not open port on %s. No UPnP device \
                found.'
                            % self.port)

        # Get our IP address
        self.public_addr = u.externalipaddress()
        self.private_addr = u.lanaddr

        success = u.addportmapping(self.port, self.proto,
                                   self.private_addr, self.port,
                                   'FlightAware %s at %s:%s' %
                                   (self.proto, self.private_addr,
                                    self.port), '')

        if success:
            logging.info("FA: Opened port %s on %s to %s." % (
                             self.port, self.public_addr,
                             self.private_addr))
        else:
            raise Exception('FA', 'Could not open port on %s.' % self.port)

    def _close_port(self):
        u = UPnP()
        ndevices = u.discover()
        try:
            u.selectigd()
            u.deleteportmapping(self.port, self.proto)
        except:
            logging.info("You can now close port %d." % self.port)

    def _get_public_addr(self):
        r = requests.get(r'http://jsonip.com')
        self.public_addr = r.json()['ip']

    """
    Get waypoints from a cached file or from the data store.
    """
    def _get_waypoints(self, fid):
        trackfile = "data/tracks/%s.json" % fid
        waypoints = None
        try:
            os.makedirs("data/tracks")
        except:
            pass

        try:
            waypoints = json.load(open(trackfile, "r"))
            logging.debug("FA: Cache hit for waypoints for %s." % fid)
        except:
            logging.debug("FA: Cache miss for waypoints for %s." % fid)
            # returns "no data found"
            # data = self.source.decode_flight_route(fid)

            # returns "" for waypoints outside the US
            # data = self.source.in_flight_info(fid)

            # for in air returns the track so far
            try:
                waypoints = self.source.get_last_track(fid)
            # updateType — data source of last position
            # (TP=projected, TO=oceanic, TZ=radar, TA=broadcast).
            # TP has no groudspeed or alitude
            #

            # If you are interested in developing your own application using
            # the FlightXML API, you can use the "GetLastTrack" or
            # "GetHistoricalTrack" function to retrieve the data and output it
            # in any format you like. Timestamps are provided in UNIX epoch
            # format (seconds since 1970).

                self._update_cost(2)
                with open(trackfile, 'w') as outfile:
                    json.dump(waypoints, outfile, indent=2)
            except:
                # @TODO catch the not implemented error
                # @TODO catch max retries
                # @TODO catch other network related errors
                sys.stderr.write("Couldn't get waypoints from FA API. Possibly the Python library is out of date. Check the fork.\n")
        return waypoints

    """
    Get airport information by IATA iatacode.
    """
    def _get_airport(self, iatacode):
        # This data structure could be shelved along with the other data
        try:
            airports = json.load(open("data/fa-airports.json", "rb"))
            json.dumps(airports, indent=2)
            logging.debug("%d airports loaded.\n" % len(airports))
        except:
            logging.info("Airports file will be created.")
            airports = {}

        if iatacode not in airports:
            logging.info("Looking up %s." % iatacode)
            info = self.source.airport_info(iatacode)
            self._update_cost(3)

            # save it to our local database
            airports[iatacode] = info

            with open("data/fa-airports.json", 'w') as outfile:
                json.dump(airports, outfile, indent=2)

        return airports[iatacode]

    """
    Transformations to perform on the data before allowing it out.
    """
    def _after_data(self, data):
        if self.track:
            for d in data:
                if "waypoints" in d and not d["waypoints"] and d["faFlightID"]:
                        # fid = d["faFlightID"]
                        # or ident@departureTime
                    d["waypoints"] = self.get_waypoints(d)
                elif d["ident"]:
                    d["waypoints"] = self.get_waypoints(d)
        return data

    """
    Utilities

    * DecodeFlightRoute: From an FID get the proposed route (not actual).
    """

    def get_waypoints(self, flight):
        """
        @TODO: What we could do is store our own waypoints over time rather
        than loading expensive points here.
        """
        # fid = flight["faFlightID"]
        # for decode_flight_route
        # fid = "%s@%s" % (flight["ident"], flight["departureTime"])
        # for in_flight_info and get_last_track
        fid = flight["ident"]
        # Check the cache
        data = self._get_waypoints(fid)
        return data

    def message(self, msg, level='info'):
        logging.info(bcolors.OKBLUE + msg + bcolors.ENDC)

    def stop(self):
        if self.update_type == "push":
            self._clear_pushes()
            self._stop_server()
            self._close_port()

        logging.info("FA: Running cost on %d queries: $%f." %
                        (self.queries_running, self.cost_running))
        self.config = shelve.open("data/fa-storage")
        self.config["cost_total"] += self.cost_running
        self.config["queries_total"] += self.queries_running
        logging.info("FA: Total cost on %d queries: $USD%f." %
                        (self.config["queries_total"],
                         self.config["cost_total"]))

        self.config.close()


def main():
    fa = RX_FlightAware()

    while True:
        fa.update()
        logging.info("%s" % fa.update_last)
        time.sleep(fa.update_throttle)


if __name__ == "__main__":
    main()

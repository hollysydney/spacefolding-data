import sys
import json
import time
from datetime import datetime

from .receiver import Receiver


class RX_Logger(Receiver):
    prefix = ["logger"]
    update_throttle = 0.1

    def __init__(self, filename="data/spacefolding.log", prefix=prefix):
        super().__init__()
        # self.update_last = datetime.datetime.now()
        self.log_last = None
        self.filename = filename
        # open the file for reading
        self.logfile = open(filename)
        self.prefix = prefix

        self.timeformat = '%Y-%m-%d-%H:%M:%S'

    def update(self):
        self.data = []

        # read the next bit of the file
        line = self.logfile.readline()

        # detect the time
        dt = datetime.strptime(line.split()[0], self.timeformat)
        if not self.log_last:
            self.log_last = dt

        # sleep for a bit
        # how long between lines?
        interval = dt - self.log_last
        # print("LOG: Sleeping for %f." % interval.total_seconds())
        time.sleep(interval.total_seconds())

        # set the updated data
        try:
            self.data = json.loads(" ".join(line.split()[1:]))
        except:
            sys.stderr.write("%s: That's not JSON.\n" % dt)

        # update the last log time
        self.log_last = dt

    def stop(self):
        # close file
        super().stop()
